# Phase 1 goals

### Feature wishlist
  * basic functions - add/edit/delete galleries and photos
  * EXIF reading functions
  * private viewing area for clients - AWS Cognito
  * Bulk upload capability -
    * read from html/local folder, allow for recursion
    * read EXIF, text file, or null to place in categories, title, description, and other attributes including EXIF data such as aperture/shutter/ISO, as well as author, copyright, and more.  Text file approach can also define desired max resolutions such as
      * thumbnail 150x150
      * mobile size 320x240 up to maybe 720?
      * desktop size 640x480 up to 1920x1080
      * print - max resolution - not available for viewing/download, but used for sending to print shop
  * comments
    * toggle to allow/block comments by photo or gallery,
    * tools to manage, publish, and delete comments
    * tools to block users from commenting by user name or IP
    * settings to only allow logged in users (optional)
  * Themes
    * allow for custom themes to be uploaded/managed in the UI
      - allow for different scales on mobile/desktop
      - allow for colors/branding/logo changes as needed.
      - allow for varied design approaches such as 1, 2, or 3 column layouts, specifying the column width as a percent
      - support mobile separately as needed.