# vue-photo-slide

Goal - To provide a simple, easy to use UI interface/tool to allow non-technical people such as photographers to create a domain, upload their 'site' to said domain, publish photos, blogs, allow for comments, private galleries, and configure settingts such as themes.

### Idea ###
The primary idea behind vue-photo-bucket is that there are few good options for photographers to display and promote their work.  Back in the day we had decent tools like Flickr, and eventually facebook, and instagram was originally targeted at photography for display and promotion.  However, as large corporations took over and grew those resources and modified them, the original intent was changed to follow fads, content creators lacking substance but focused on the now.  Today there are hundreds if not thousands of photographers wondering why they have to fight the algorithms in social media just to get people to see their work.  This detracts from a photographer's need to promote their work and time away from taking photos.  Photo galleries in and of themselves are not complex or complicated applications, but you shouldn't have to pay a premium to promote your work and you shouldn't have to fight a large corporation that's more interested in promoting fads either.
There are a handful of features I want vue-photo-bucket to have to make it easier for the average person that's more interested in photography and less interested in computers to be able to create and manage their portfolio in an easy, almost simplistic manner.

### Design & Construction ###
--- this may not end up in the final readme, just a place for notes ---
I'll create a P1, P2, and P3 readme for each phase of development

### Feature wishlist
  * nesting categories - no depth limit?  (SEO may require one)
  * basic functions - add/edit/delete galleries and photos
  * EXIF reading functions
  * private viewing area for clients - AWS Cognito
  * file upload & resizing
  * Bulk upload capability -
    * read from html/local folder, allow for recursion
    * read EXIF, text file, or null to place in categories, title, description, and other attributes including EXIF data such as aperture/shutter/ISO, as well as author, copyright, and more.  Text file approach can also define desired max resolutions such as
      * thumbnail 150x150
      * mobile size 320x240 up to maybe 720?
      * desktop size 640x480 up to 1920x1080
      * print - max resolution - not available for viewing/download, but used for sending to print shop
  * printing
    * allow for print services (expansion) with setup/configuration, contact print services via API to order prints
    * allow for local at home printing orders
  * comments
    * toggle to allow/block comments by photo or gallery,
    * tools to manage, publish, and delete comments
    * tools to block users from commenting by user name or IP
    * settings to only allow logged in users (optional)
  * Themes
    * allow for custom themes to be uploaded/managed in the UI
      - allow for different scales on mobile/desktop
      - allow for colors/branding/logo changes as needed.
      - allow for varied design approaches such as 1, 2, or 3 column layouts, specifying the column width as a percent
      - support mobile separately as needed
  * Global Gallery
    * An opt-in service where new sites are logged with a master app with photographer name, site url, contact info (email/phone -optional), select thumbnails, etc.
      * The global gallery should function as an API call, where all sites that participate in the global gallery are required to display a link to the global gallery.  There users can search and browse other photograhers by name, genre, and region to start.
      * if the GG setting is toggled on, an API call should be sent informing the master GG to accept this site as a member of the GG.  A response API hash should be decoded into a json object with the GG index for presenting within the theme of the existing site.
  * backend info
    * UI should be written entirely in Vue.js
    * backend can be Laravel(PHP), Node (express), or Golang
    * database should primarily be json objects but allow for configuring a full database such as MariaDB, Mysql, and Postgres as well as AWS Aurora.  (NoSQL isn't needed)
    * site is intended to run either on AWS Lambda or Purely on AWS S3 Static hosting
    * will need tools to automatically connect and deploy accordingly
    * include an admin dashboard to use along with AWS tools to show site visits, orders, etc.


bulk upload file format - I'm preferring to use EXIF but not everyone uses or edits that info

1 file for all images or 1 file per image

similar to json or yaml, just text key/value pairs

image
  filename: xxx.jpg
  date: xx-xx-xxxx  (mm/dd/yyyy or dd/mm/yyyy)
  name: my pic
  description: a picture of me I took
--- optional below here---
camera
  brand
  lens
settings
  ISO
  Aperture
  shutterspeed
film/digital
film brand/type
location
  city/town/region name
  country
  lat/long
People
  name/gender/age
  name/gender/age


plugins notes

https://github.com/RyanClementsHax/tailwindcss-themer
https://stackoverflow.com/questions/69150928/how-to-create-multiple-themes-using-tailwind-css
https://github.com/sagold/json-schema-library
https://dev.to/pqina/generating-image-thumbnails-in-the-browser-using-javascript-and-filepond-10b8
https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/

List Albums/View photos
https://docs.aws.amazon.com/sdk-for-javascript/v3/developer-guide/s3-example-photos-view.html

Upload Albums/Photos
https://docs.aws.amazon.com/sdk-for-javascript/v3/developer-guide/s3-example-photo-album.html


Done/Completed

- base tailwind/vue install -- the plan is to use AWS for a lot of the heavy lifting.  S3 static hosting, cloudfront cdn, aws certificates for ssl, route53 for dns, AWS Cognito for logins, consider offering static file db or dynamo db as an extension for sites over a certain size.

Questions/design decisions

-- db go with a single table schema for all images - one for categories/galleries, etc?  Or a more complex design pattern?