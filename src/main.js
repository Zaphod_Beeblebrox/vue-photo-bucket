import Vue from 'vue';
import App from './App.vue';
import router from '@/resources/router.js';
import store from '@/resources/store.js';
import '@/assets/css/main.css';

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
