import Vue from "vue";
import Router from "vue-router";
import Index from "@/Pages/Index.vue";
import TopNav from "@/Components/Navigation/TopNav.vue";
import About from '@/Pages/About.vue';
import Contact from '@/Pages/Contact.vue';

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "index",
      components: {
        nav: TopNav,
        default: Index,
      },
    },
    {
      path: "/about",
      name: "About",
      components: {
        nav: TopNav,
        default: About,
      },
    },
    {
      path: "/contact",
      name: "Contact",
      components: {
        nav: TopNav,
        default: Contact,
      },
    },
  ],
});
