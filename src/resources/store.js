import Vue from 'vue';
import Vuex from 'vuex';

// json data
import NavMenu from '@/resources/data/navigation.json';
import Site from '@/resources/data/site.json';

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        navigation: NavMenu,
        site: Site,  
        mobileMenu: false,
    },
    mutations: {
        TOGGLEMENU (state) {
            state.mobileMenu = !state.mobileMenu;
        }
    },
    actions: {
        setMenuState ( {commit} ) {
            console.log('here');
            commit('TOGGLEMENU');
        }
    },
    getters: {
       menu(state) {
            return state.navigation;
       },
       siteName(state) {
            return state.site.site_name;
       },
       aboutMe(state) {
            return state.site.about
       },
       mobileMenu(state) {
        return state.mobileMenu;
       }
    }
})
